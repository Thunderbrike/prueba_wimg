﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour
{

    public float velocity;
    float counter;
    Quaternion rotation = Quaternion.identity;

    void Start()
    {

    }

    void Update()
    {
        counter += Time.deltaTime * velocity;
        rotation.eulerAngles = new Vector3(-90, counter, 0);
        transform.rotation = rotation;
    }
}
